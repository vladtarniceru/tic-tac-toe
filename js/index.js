const StyleConstants = {
  COLOR_BORDER: '#232634',
  COLOR_FONT: '#8F93A2',
  COLOR_SQUARES: {
    RED: '#E06567',
    BLUE: '#7097EA',
    GREEN: '#94C079',
    YELLOW: '#F2C668',
    ORANGE: '#F58969',
  },
};
const n = 3;
const CellType = {
  EMPTY: 1,
  X: 2,
  O: 3,
};
let grid
let turn;
let winner;


reloadButton = document.createElement('div');
reloadButton.innerHTML = 'Reload?';
reloadButton.id = 'reloadButton';
reloadButton.onclick = () => {
  Game.init();
};


Utils = {
  removeAllEventListeners: (element) => {
    let cloneElement = element.cloneNode();
    while (element.firstChild) {
      cloneElement.appendchild(element.lastChild);
    }
    element.parentNode.replaceChild(cloneElement, element);
  },

  checkSameNotEmpty: (array) => {
    for (let i = 0; i < n; i += 1) {
      if (array[i].cellType !== array[0].cellType 
        || array[i].cellType === CellType.EMPTY) {
        return false;
      }
    }
    return true;
  },
};


Game = {
  init: () => {
    grid = [];
    for (let i = 0; i < n; i += 1) {
      grid.push(Array(n));
    }
    turn = 1;
    winner = false;
    document.getElementById('grid').innerHTML = '';
    document.getElementById('turn').innerHTML = '';
    Game.reloadTurnElement();
    Game.renderScore();
    Game.initGrid();
    Game.renderGrid();
  },

  characterAtTurn: () => {
    if (turn % 2 === 1) {
      return 'X';
    }
    return 'O';
  },

  reloadTurnElement: () => {
    let turnElement = document.getElementById('turn');
    if (!winner && turn !== 9) {
      turnElement.innerHTML = `
        Turn ${turn}.<br />
        '${Game.characterAtTurn()}' to move.<br />
      `;
    } else {
      turnElement.innerHTML = `
        Game Over.<br />
      `;
      if (!winner && turn === 9) {
        turnElement.innerHTML += `
          It's a draw.<br />
        `;
      } else if (winner) {
        turnElement.innerHTML += `
          The winner is '${Game.characterAtTurn()}'.<br />
        `;
      }
      turnElement.appendChild(reloadButton);
    }
  },

  handleClick: (cellElement) => {
    if (cellElement.cellType != CellType.EMPTY) {
      console.error('You must pick an empty cell');
      return;
    }
    let character = Game.characterAtTurn();
    cellElement.innerHTML = character;
    cellElement.cellType = CellType[character];
    cellElement.style['color'] = 
      character === 'X' 
      ? localStorage.getItem('xColor')
      : localStorage.getItem('oColor');
    winner = Game.checkForWinner();
    if (winner || turn === 9) {
      Game.onGameOver();
    }
    Game.reloadTurnElement();
    turn += 1;
    Game.renderGrid();
  },

  onGameOver: () => {
    for (let line = 0; line < n; line += 1) {
      for (let column = 0; column < n; column += 1) {
        grid[line][column].onclick = null;
      }
    }
    const localStorageItem = `score${Game.characterAtTurn()}`;
    if (winner) {
      localStorage.setItem(localStorageItem, parseInt(localStorage.getItem(localStorageItem)) + 1);
      localStorage.score[Game.characterAtTurn()] += 1;
    }
  },

  checkForWinner: () => {
    for (let line = 0; line < n; line += 1) {
      if (Utils.checkSameNotEmpty(grid[line])) {
        return true;
      }
    }
    for (let column = 0; column < n; column += 1) {
      if (Utils.checkSameNotEmpty([grid[0][column], grid[1][column], grid[2][column]])) {
        return true;
      }
    }
    if (Utils.checkSameNotEmpty([grid[0][0], grid[1][1], grid[2][2]])) {
      return true;
    }
    if (Utils.checkSameNotEmpty([grid[0][2], grid[1][1], grid[2][0]])) {
      return true;
    }
    return false;
  },

  createCell: (line, column, cellType) => {
    let cellElement = document.createElement('div');
    const addBorder = (direction) => {
      cellElement.style[`border-${direction}`] = `1px solid ${StyleConstants.COLOR_BORDER}`;
    };
    cellElement.line = line;
    cellElement.column = column;
    cellElement.cellType = cellType;
    cellElement.className = 'gridCell';
    cellElement.onclick = (event) => Game.handleClick(event.target);
    if (line != 0) {
      addBorder('top');
    }
    if (line != n - 1) {
      addBorder('bottom');
    }
    if (column != 0) {
      addBorder('left');
    }
    if (column != n - 1) {
      addBorder('right');
    }
    return cellElement;
  },

  initGrid: () => {
    for (let line = 0; line < n; line += 1) {
      for (let column = 0; column < n; column += 1) {
        grid[line][column] = Game.createCell(line, column, CellType.EMPTY);
      }
    }
  },

  renderGrid: () => {
    let gridElement = document.getElementById('grid');
    for (let line = 0; line < n; line += 1) {
      for (let column = 0; column < n; column += 1) { 
        gridElement.appendChild(grid[line][column]);
      }
    }
  },

  renderScore() {
    if (!localStorage.getItem('xColor')) {
      localStorage.setItem('scoreX', 0);
      localStorage.setItem('scoreO', 0);
      localStorage.setItem('xColor', StyleConstants.COLOR_FONT);
      localStorage.setItem('oColor', StyleConstants.COLOR_FONT);
    }
    let statsElement = document.getElementById('stats');
    statsElement.innerHTML = `${localStorage.getItem('scoreX')} - ${localStorage.getItem('scoreO')}`;
  }
};


let ColorsPanel = {
  createColorSquare: (color, target) => {
    let squareElement = document.createElement('div');
    squareElement.className += ' square';
    squareElement.style['background-color'] = color;
    squareElement.onclick = () => {
      localStorage.setItem(target, color);
    }
    return squareElement;
  },

  addColorSquares: (target) => {
    let colorBarElement = document.getElementById('colorBar');
    let container = document.createElement('div');
    container.style['display'] = 'flex';
    for (let color in StyleConstants.COLOR_SQUARES) {
      if (!StyleConstants.COLOR_SQUARES.hasOwnProperty(color)) {
        continue;
      }
      container.appendChild(ColorsPanel.createColorSquare(StyleConstants.COLOR_SQUARES[color], target));
    }
    colorBarElement.appendChild(container);
  }
}


window.onload = () => {
  ColorsPanel.addColorSquares('xColor');
  ColorsPanel.addColorSquares('oColor');
  Game.init();
};